/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf.result;

import de.mn77.base.data.convert.ConvertArray;


/**
 * @author Michael Nitsche
 * @created 23.02.2021
 */
public class MIniConfObject {

	public final String   type;
	public final String[] args;


	public MIniConfObject( final String type, final String[] args ) {
		this.type = type;
		this.args = args;
	}

	@Override
	public String toString() {
		return this.type + '(' + ConvertArray.toString( ",", (Object[])this.args ) + ')';
	}

}
