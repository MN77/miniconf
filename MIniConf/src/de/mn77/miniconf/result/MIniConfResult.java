/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf.result;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import de.mn77.miniconf.util.Lib_Result;


/**
 * @author Michael Nitsche
 * @created 22.02.2021
 */
public class MIniConfResult {

	private final HashMap<String, String> entrys = new HashMap<>();


	public void addEntry( final String path, final String value ) {
		this.entrys.put( path, value );
	}

	public Boolean getBoolean( final String path ) {
		return this.getBoolean( path, null );
	}

	public Boolean getBoolean( final String path, final Boolean ifAbsent ) {
		String value = this.getString( path, null );
		if( value == null )
			return ifAbsent;
		else
			value = value.toLowerCase();
		if( value.length() == 0 || value.equals( "0" ) || value.equals( "false" ) || value.equals( "no" ) )
			return false;
		if( value.startsWith( "0." ) && value.matches( "0\\.[0]*" ) )
			return false;
		return true;
	}

	public Double getDouble( final String path ) {
		return this.getDouble( path, null );
	}

	public Double getDouble( final String path, final Double ifAbsent ) {
		final String value = this.getString( path, null );
		if( value == null )
			return ifAbsent;

		try {
			return Double.parseDouble( value );
		}
		catch( final NumberFormatException t ) {
			return ifAbsent;
		}
	}

	public Integer getInteger( final String path ) {
		return this.getInteger( path, null );
	}

	public Integer getInteger( final String path, final Integer ifAbsent ) {
		final String value = this.getString( path, null );
		if( value == null )
			return ifAbsent;

		try {
			return Integer.parseInt( value );
		}
		catch( final NumberFormatException t ) {
			return ifAbsent;
		}
	}

	public Set<Entry<String, String>> getItems() {
		return this.entrys.entrySet();
	}

	public Set<String> getKeys() {
		return this.entrys.keySet();
	}

	public String[] getList( final String path ) {
		return this.getList( path, null );
	}

	public String[] getList( final String path, final String[] ifAbsent ) {
		final String value = this.getString( path, null );
		if( value == null )
			return ifAbsent;
		return Lib_Result.parseList( value, '[', ']' );
	}

	public MIniConfObject getObject( final String path ) {
		return this.getObject( path, null );
	}

	public MIniConfObject getObject( final String path, final MIniConfObject ifAbsent ) {
		final String value = this.getString( path, null );
		if( value == null )
			return ifAbsent;

		final int gap = value.indexOf( '(' );
		final String type = Lib_Result.checkType( gap > -1 ? value.substring( 0, gap ) : value );
		final String[] args = gap > -1 ? Lib_Result.parseList( value.substring( gap ), '(', ')' ) : new String[0];
		return type == null || args == null ? ifAbsent : new MIniConfObject( type, args );
	}

	public String getString( final String path ) {
		return this.getString( path, null );
	}

	public String getString( final String path, final String ifAbsent ) {
		final String key = Lib_Result.normalizePath( path );
		if( !this.entrys.containsKey( key ) )
			return ifAbsent;
		final String value = this.entrys.get( key );
		return Lib_Result.parseString( value );
	}

}
