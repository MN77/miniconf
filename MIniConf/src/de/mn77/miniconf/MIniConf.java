/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf;

import java.io.File;
import java.nio.file.Path;

import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.miniconf.parser.ScriptParser;
import de.mn77.miniconf.result.MIniConfResult;


/**
 * @author Michael Nitsche
 * @created 07.03.2021
 */
public class MIniConf {

	public static MIniConfResult parse( final File source ) throws Err_FileSys {
		final String s = Lib_TextFile.read( source );
		return MIniConf.parse( s );
	}

	public static MIniConfResult parse( final Path source ) throws Err_FileSys {
		final String s = Lib_TextFile.read( source.toFile() );
		return MIniConf.parse( s );
	}

	public static MIniConfResult parse( final String source ) {
		return ScriptParser.parse( source );
	}

}
