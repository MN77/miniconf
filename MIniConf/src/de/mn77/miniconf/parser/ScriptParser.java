/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf.parser;

import java.util.ArrayList;
import java.util.List;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.error.Err;
import de.mn77.miniconf.result.MIniConfResult;
import de.mn77.miniconf.util.Lib_MIniConf;


/**
 * @author Michael Nitsche
 * @created 18.02.2020
 */
public class ScriptParser {

	public static MIniConfResult parse( final String s ) {
		final MIniConfResult result = new MIniConfResult();
		final List<String> lines = ConvertString.toList( '\n', s );

		final ArrayList<String> path = new ArrayList<>();
		boolean blockComment = false;

		for( final String line2 : lines ) {
			final String line = line2.trim();
//			MOut.line(line);

			if( line.length() == 0 )
				continue;

			final char c0 = line.charAt( 0 );

			// --- comments ---

			if( line.equals( "*/" ) ) {
				blockComment = false;
				continue;
			}

			if( blockComment || c0 == '#' || c0 == ';' )
				continue;

			if( c0 == '/' && line.startsWith( "/*" ) ) {
				blockComment = true;
				continue;
			}

			// --- headlines ---

			if( c0 == '[' ) {
				ScriptParser.iPathBracket( path, line );
				continue;
			}

			if( c0 == '*' ) {
				ScriptParser.iPathStar( path, line );
				continue;
			}

			if( c0 == '.' ) {
				ScriptParser.iPathDot( path, line );
				continue;
			}

			// --- entrys ---

//			buffer =
			ScriptParser.iEntry( result, path, line );
		}

		// Clean up
//		result.addEntry(ScriptParser.iKey(path,key), list != null ? list : value);

		return result;
	}

	private static void iEntry( final MIniConfResult result, final ArrayList<String> path, final String line ) {
		int equalIndex = line.indexOf( '=' );

		if( equalIndex == -1 )
			equalIndex = line.indexOf( ':' );
		if( equalIndex == -1 )
			Err.todo( line );

		final String key = line.substring( 0, equalIndex ).trim();
		final String v = line.substring( equalIndex + 1 ).trim();
		String value = v.length() == 0 ? null : v;
		value = Lib_MIniConf.cutLineComment( value ).trim();
		result.addEntry( ScriptParser.iKey( path, key ), value );
	}

	private static String iKey( final ArrayList<String> path, final String key ) {
		Lib_MIniConf.checkKey( key );
		final StringBuilder sb = new StringBuilder();

		for( final String p : path ) {
			if( sb.length() > 0 )
				sb.append( "." );
			sb.append( p );
		}
		if( sb.length() > 0 )
			sb.append( "." );
		sb.append( key );
		return sb.toString().toLowerCase();
	}

	private static void iPathBracket( final ArrayList<String> path, final int depth1, String line ) {
		final int end = line.indexOf( ']' ); // All following will be ignored!
		line = end <= -1
			? line.substring( 1 )
			: line.substring( 1, end );

		final int depth2 = Lib_MIniConf.depth( line, '.' );
		ScriptParser.iSetPath( path, 1 + depth1 + depth2, line.substring( depth2 ).trim() );
	}

	private static void iPathBracket( final ArrayList<String> path, final String line ) {
		ScriptParser.iPathBracket( path, 0, line.trim() );
	}

	private static void iPathDot( final ArrayList<String> path, final String line ) {
		final int dots = Lib_MIniConf.depth( line, '.' );
		ScriptParser.iPathBracket( path, dots, line.substring( dots ).trim() );
	}

	private static void iPathStar( final ArrayList<String> path, final String line ) {
		final int stars = Lib_MIniConf.depth( line, '*' );
		ScriptParser.iSetPath( path, stars, line.substring( stars ).trim() );
	}

	/**
	 * depth = 1-
	 */
	private static void iSetPath( final ArrayList<String> path, final int depth, final String headline ) {
		// TODO Punkte innerhalb der Zeile!!!

		// Trim
		while( path.size() < depth )
			path.add( "_" );
		while( path.size() > depth )
			path.remove( path.size() - 1 );

		path.set( depth - 1, headline );
	}

	/*
	 * if(list != null && value != null)
	 * list.add(value);
	 * result.addEntry(ScriptParser.iKey(path,key), list != null ? list.toArray(new String[list.size()]) : value);
	 */
}
