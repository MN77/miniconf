/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf._test;

import java.nio.file.Path;
import java.nio.file.Paths;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.miniconf.MIniConf;
import de.mn77.miniconf.result.MIniConfResult;


/**
 * @author Michael Nitsche
 * @created 22.02.2021
 */
public class Test_2 {

	public static void main( final String[] args ) {
		MOut.setDebug();
//		MOut.debugNo();

		try {
			final Path source = Paths.get( "/home/mike/git/config-lang/MOCL/src/de/mn77/miniconf/_test/example3.mconf" );

			final Stopwatch timer = new Stopwatch();
			final MIniConfResult result = MIniConf.parse( source );

			MOut.print( "==================================================" );
			MOut.print( "= Keys:" );
			result.getKeys().forEach( e -> MOut.print( e ) );

			MOut.print( "==================================================" );
			MOut.print( "= Items:" );
			result.getItems().forEach( ( e ) -> {
				final Object val = e.getValue();
				MOut.print( e.getKey() + " --> " + ConvertObject.toStringIdent( val ) + " (" + (val == null ? "null" : val.getClass().getSimpleName()) + ')' );
			} );

			MOut.print( "==================================================" );
			MOut.print( "= Types:" );
			MOut.print( "--> " + ConvertObject.toStringIdent( result.getString( "abc.xyz.wqt" ) ) );
			MOut.print( "--> " + ConvertObject.toStringIdent( result.getDouble( "abc.xyz.wqt" ) ) );
			MOut.print( "--> " + ConvertObject.toStringIdent( result.getBoolean( "abc.xyz.wqt" ) ) );

			MOut.print( "--> " + ConvertObject.toStringIdent( result.getString( "abc.xyz.alpha.ss2" ) ) );

			MOut.print( "--> " + ConvertObject.toStringIdent( result.getString( "abc.wer.hobbys.list" ) ) );
			MOut.print( "--> " + ConvertObject.toTextDebug( result.getList( "abc.wer.hobbys.list" ) ) );

			MOut.print( "--> " + ConvertObject.toStringIdent( result.getString( "abc.xyz.alpha.zrt" ) ) );
			MOut.print( "--> " + ConvertObject.toStringIdent( result.getObject( "abc.xyz.alpha.zrt" ) ) );


			timer.print();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
