/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf._test;

import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.miniconf.MIniConf;
import de.mn77.miniconf.result.MIniConfObject;
import de.mn77.miniconf.result.MIniConfResult;


/**
 * @author Michael Nitsche
 * @created 22.02.2021
 */
public class Test_1b {

	public static void main( final String[] args ) {
		MOut.setDebug();

		try {
			//		Path source = Paths.get("/path/to/file.toml");
			//		String source = "int3 = 42";
			final String source = "bool1 = true\nBool2 = FALSE\nbool3=0\n bool4=True\nobj1=Obj(\"foo\",6, 123)"; // nil1=nil\n
			MOut.print( source, "" );

			final Stopwatch timer = new Stopwatch();

			final MIniConfResult result = MIniConf.parse( source );
//			result.getErrors().forEach(error -> System.err.println(error.toString()));	// TODO

			MOut.print( result.getString( "a.  	dotted . key", "foo" ) );
			MOut.print( result.getString( "bool1" ) );
			MOut.print( result.getString( "bool2" ) );
//			MOut.print( result.getString("nil1") );

			final MIniConfObject obj = result.getObject( "obj1" );
			MOut.print( obj.type, obj.args );
			timer.print();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

}
