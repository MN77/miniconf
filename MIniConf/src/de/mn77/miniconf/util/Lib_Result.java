/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf.util;

import java.util.ArrayList;


/**
 * @author Michael Nitsche
 * @created 11.03.2021
 */
public class Lib_Result {

	/**
	 * Check and trim object type.
	 * If type contains invalid chars, than 'null' will be returned.
	 * Valid chars are: A-Z,a-z,0-9,_,.
	 *
	 * @return trimmed and lowered type. 'null' will be returned on errors
	 */
	public static String checkType( String type ) {
		type = type.trim();

		for( final char c : type.toCharArray() ) {
			if( c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' )
				continue;
			if( c >= '0' && c <= '9' || c == '_' || c == '.' )
				continue;
			return null;
		}
		return type;
	}

	public static String normalizePath( final String path ) {
		return path.toLowerCase().replace( " ", "" ).replace( "\t", "" );
	}

	/**
	 * @return First value can be String or String[]. Second value is the last parsed char
	 */
	public static String[] parseList( String line, final char open, final char close ) {
		line = line.trim();
		if( line.length() == 0 )
			return new String[0];
		if( line.charAt( 0 ) != open )
			return null;
		line = line.substring( 1 );

		final ArrayList<Object> result = new ArrayList<>();
		StringBuilder value = new StringBuilder();

		boolean openQuote1 = false;
		boolean openQuote2 = false;
		boolean escape = false;
		boolean content = false;
		int start = 0;
		int end = 0;

		for( int idx = 0; idx < line.length(); idx++ ) {
			final char c = line.charAt( idx );

			if( c == '\\' )
				escape = true;
			else if( escape ) {
				value.append( c );
				content = true;
				end = value.length();
			}

			else if( !openQuote1 && c == '"' )
				openQuote2 = !openQuote2;
			else if( !openQuote2 && c == '\'' )
				openQuote1 = !openQuote1;
			else if( openQuote1 || openQuote2 ) {
				value.append( c );
				content = true;
				end = value.length();
			}

			else if( c == ',' ) {
				result.add( value.toString().substring( start, end ) );
				value = new StringBuilder();
				start = 0;
				end = 0;
				content = false;
			}
			else if( c == ' ' || c == '\t' ) {
				if( !content )
					start++;
				value.append( c );
			}

			else if( c == ')' ) {
				result.add( value.toString().substring( start, end ) );
				return result.toArray( new String[result.size()] );
			}

			else {
				value.append( c );
				end = value.length();
				content = true;
			}
		}

		result.add( value.toString().substring( start, end ) );
		return result.toArray( new String[result.size()] );
	}

	public static String parseString( final String value ) {
		final int len = value.length();
		if( len == 0 )
			return value;
		final char c0 = value.charAt( 0 );
		if( c0 == '"' )
			return Lib_Result.removeQuote( value, '"' );
		if( c0 == '\'' )
			return Lib_Result.removeQuote( value, '\'' );
		return value;
	}

	/**
	 * Lazy = Trailing quote can be missing
	 */
	public static String removeQuote( String value, final char c ) {
		value = value.substring( 1 );
		return value.endsWith( "" + c ) ? value.substring( 0, value.length() - 1 ) : value;
	}

}
