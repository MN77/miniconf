/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the MIniConf library <https://gitlab.com/MN77/miniconf>
 *
 * MIniConf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MIniConf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MIniConf. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.mn77.miniconf.util;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 11.03.2021
 */
public class Lib_MIniConf {

	public static void checkKey( final String key ) {
		for( final char c : key.toCharArray() )
			if( c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_' ) {}
			else
				Err.invalid( key );
	}

	public static int depth( final String line, final char leading ) {
		int depth = 0;
		for( final char c : line.toCharArray() )
			if( c == leading )
				depth++;
			else
				break;
		return depth;
	}

	public static String cutLineComment( String value ) {
		boolean openSingle = false;
		boolean openDouble = false;
//		boolean openBlock = false; // Line-Block-Comment

		for( int i = 0; i< value.length(); i++ ) {
			final char c = value.charAt( i );
			if( c == '\'')
				openSingle = !openSingle;
			else if( c == '"')
				openDouble = !openDouble;
			else if( c == '#' && !openSingle && !openDouble )
				return value.substring( 0, i );
		}

		return value;
	}

}
